﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(detyreASPmvc.Startup))]
namespace detyreASPmvc
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
