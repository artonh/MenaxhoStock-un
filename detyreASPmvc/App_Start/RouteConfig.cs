﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace detyreASPmvc
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "ProductIDWithSender",
            //    url: "{controller}/{action}/{id}/{Quantity}",
            //    defaults: new { controller = "Home", action = "KonfirmojShitjen", id = UrlParameter.Optional, Quantity=UrlParameter.Optional }
            //);
        }
    }
}
