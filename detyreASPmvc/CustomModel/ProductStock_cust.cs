﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace detyreASPmvc.CustomModel
{
    public class ProductStock_cust
    {

        public int ProdID { get; set; }
        
        [Required(ErrorMessage ="Ju lutemi mbusheni barcode-in")]
        [RegularExpression(@"^[0-9]{6,6}$", ErrorMessage = "Duhet te jete me 6 numra")]//sa per improvizim manual
        [Remote("IsBarcodeExist", "Home",ErrorMessage ="Barcode i njejte tashme ekziston!")]
        public int Barcode { get; set; }

        [DisplayName("Emri i produktit")]
        [Required(ErrorMessage = "Ju lutemi mbusheni emrin e produktit")]
        [StringLength(90, MinimumLength = 3, ErrorMessage = "Rangu 3 - 90 karaktere")] /**/
        //[RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "Duhet te jete shkronje a-zA-Z")]
        public string ProductName { get; set; }

        [DisplayName("Pershkrimi")]
        [StringLength(150, MinimumLength = 3, ErrorMessage = "Rangu 3 - 150 karaktere")] /**/
        public string Description { get; set; }
        [Required(ErrorMessage = "Ju lutemi mbusheni sasin")]
         [RegularExpression(@"^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$", ErrorMessage = "Duhet te jete numer pozitiv")]///$/  //[0-9]+$
        [DisplayName("Sasia")]
        public double Quantity { get; set; }

        [Remote("IsQuantityAvailability", "Home", AdditionalFields = "ProdID", ErrorMessage = "Kjo sasi nuk eshte e disponueshme!")]
        [Required(ErrorMessage = "Ju lutemi mbusheni sasin")]
        [RegularExpression(@"^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$", ErrorMessage = "Duhet te jete numer pozitiv")]///$/  //[0-9]+$
        [DisplayName("Sasia")]
        public double? QuantitySold { get; set; }

        [Required(ErrorMessage = "Ju lutemi mbusheni cmimin per njesi")]
        //[DataType(DataType.Currency)] per dollar
        [DisplayFormat(DataFormatString = "{0:n} €")]
        [RegularExpression(@"^[+]?([0-9]+(?:[\.][0-9]*)?|\.[0-9]+)$", ErrorMessage = "Duhet te jete numer pozitiv")]///$/  //[0-9]+$
        [DisplayName("Cmimi njesi")]
        public decimal UnitPrice { get; set; }

        [DisplayName("Data regjistrimit")]
        public DateTime DateRegister { get; set; }

        [DisplayName("Fshirja")]
        public bool IsDeleted { get; set; }

        [DisplayName("Data modifikimit")]
        [DisplayFormat(NullDisplayText = "Ende e pamodifkuar", ApplyFormatInEditMode = true)]
        public DateTime? DateModified { get; set; }

       
    }
}