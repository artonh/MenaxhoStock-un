﻿using detyreASPmvc.CustomModel;
using detyreASPmvc.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace detyreASPmvc.Controllers
{
    public class HomeController : Controller
    {
       
        public async Task<ActionResult> Index()
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                var tedhenat = from r in db.ProductStock select r;

                return View(await tedhenat.ToListAsync());
            }
        }
        public async Task<ActionResult> KerkoNeIndex(string Search, string searchBy)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                var tedhenat = from r in db.ProductStock select r;
                if (!String.IsNullOrEmpty(Search) || !String.IsNullOrEmpty(searchBy))
                {
                    Search = Search.Trim();
                    searchBy = searchBy.Trim();
                    switch (searchBy)
                    {
                        case "0":
                            tedhenat = tedhenat.Where(i => i.Barcode.ToString().StartsWith(Search));
                            break;
                        case "1":
                            tedhenat = tedhenat.Where(i => i.ProductName.ToString().StartsWith(Search));
                            break;
                        case "2":
                            tedhenat = tedhenat.Where(i => i.Description.ToString().StartsWith(Search));
                            break;
                    }
                }

                return PartialView("_Index", await tedhenat.ToListAsync());
            }
        }
        public ActionResult ShtoProduktTeRi()
        {
            return View();
        }
        [HttpPost]
        public async Task<ActionResult>ShtoProduktTeRi([Bind(Include = "ProdID,Barcode,ProductName,Description,Quantity,UnitPrice,IsDeleted,DateRegister")]ProductStock_cust _pr)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                //if (ModelState.IsValid) ///--- GABIM me model
                //{
                  
                    ProductStock ruaj = new ProductStock()
                {
                    Barcode = _pr.Barcode,
                    ProductName = _pr.ProductName,
                    Description = _pr.Description,
                    Quantity = _pr.Quantity,
                    UnitPrice = _pr.UnitPrice,
                    DateRegister = DateTime.Now,
                    IsDeleted = false                    
                };
                db.ProductStock.Add(ruaj);
                await db.SaveChangesAsync();

                return RedirectToAction("Index");
            //}
            //    else
            //    {
            //    return View(_pr);
            //}
        }
        }

        public async Task<ActionResult> Fshije(int? id)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                
                ProductStock _pr = await db.ProductStock.FindAsync(id);
                if (_pr == null)
                    return HttpNotFound();
                
                return View(_pr);
            }
        }

        [HttpPost, ActionName("Fshije")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                ProductStock _pr = await db.ProductStock.FindAsync(id);
                _pr.IsDeleted = true;
                try
                {
                    db.Entry(_pr).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                }
                catch (RetryLimitExceededException /* dex */)
                {
                    //Log the error (uncomment dex variable name and add a line here to write a log.
                    ModelState.AddModelError("",
                        "E pamundur per t'u fshire. Provo me vone,nese ka probleme te reja kontakto me Adminin. Faleminderit!");
                }
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Detale(int id)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                
                ProductStock _pr = await db.ProductStock.FindAsync(id);
                if (_pr == null)
                    return HttpNotFound();
                
                return View(_pr);
            }
        }

        public async Task<ActionResult> Edito(int? id)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                if (id == null)
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                
                ProductStock _prDB = await db.ProductStock.FindAsync(id);
                if (String.IsNullOrEmpty(id.ToString()))
                    return HttpNotFound();
                
                ProductStock_cust prCostum = new ProductStock_cust()
                {
                    ProdID = _prDB.ProdID,
                    Barcode = _prDB.Barcode,
                    ProductName = _prDB.ProductName,
                    Description = _prDB.Description,
                    Quantity = _prDB.Quantity,
                    UnitPrice = _prDB.UnitPrice,
                    IsDeleted = _prDB.IsDeleted,
                    DateRegister = _prDB.DateRegister,
                    DateModified = _prDB.DateModified
                };
                return View(prCostum);
            }
        }

        [HttpPost, ActionName("Edito")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> EditConfirmed([Bind(Include = "ProdID,Barcode,ProductName,Description,Quantity,UnitPrice,IsDeleted,DateRegister")]ProductStock_cust prCostum)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                try
                {
                    ProductStock _prDB = new ProductStock()
                    {
                        ProdID = prCostum.ProdID,
                        Barcode = prCostum.Barcode,
                        ProductName = prCostum.ProductName,
                        Description = prCostum.Description,
                        Quantity = prCostum.Quantity,
                        UnitPrice = prCostum.UnitPrice,
                        IsDeleted = prCostum.IsDeleted,
                        DateRegister = prCostum.DateRegister,
                        DateModified = DateTime.Now
                    };

                    db.Entry(_prDB).State = EntityState.Modified;
                   await db.SaveChangesAsync();

                    return RedirectToAction("Index");
                }
                catch (Exception ex /* dex */)
                {
                    ModelState.AddModelError("",
                        "E pamundur per t'u edituar. Provo me vone, nese ka probleme te reja kontakto me Adminin. Faleminderit!");
                }

                return View(prCostum);
            }
        }

        public JsonResult IsBarcodeExist(int Barcode)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                return Json(!db.ProductStock.Any(i => i.Barcode == Barcode), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult IsQuantityAvailability(double QuantitySold, int ProdID)
        {
            using (db_TeDhenat db = new db_TeDhenat())
            {
                double _Quantity;
                bool ValidResult;
                double _QuantityChosen = QuantitySold; //inicializohet me parametrin qe vjen
                _Quantity = db.ProductStock.Where(c => c.ProdID == ProdID).Select(c => c.Quantity).FirstOrDefault();

                if (_Quantity >= _QuantityChosen) ValidResult = true;
                else ValidResult = false;
                return Json(ValidResult, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";
            return View();
        }
        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}