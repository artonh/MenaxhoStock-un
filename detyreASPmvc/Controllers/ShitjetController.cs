﻿using detyreASPmvc.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Description;
using detyreASPmvc.CustomModel;

namespace detyreASPmvc.Controllers
{
    public class ShitjetController : Controller
    {
        // GET: Shitjet
        db_TeDhenat db = new db_TeDhenat();
        
        public ActionResult Shitja()//shitjaKerko
        {
            var tedhenat = from r in db.ProductStock select r;
            
            return View("Shitja", tedhenat.ToList());
        }

        [HttpPost]
        public ActionResult KerkoProdDisponueshme(string Search, string searchBy)//shitjaKerko
        {
            var tedhenat = from r in db.ProductStock select r;
            if (!String.IsNullOrEmpty(Search) || !String.IsNullOrEmpty(searchBy))
            {
                Search = Search.Trim();
                searchBy = searchBy.Trim();
                switch (searchBy)
                {
                    case "0": tedhenat = tedhenat.Where(i => i.Barcode.ToString().StartsWith(Search)); break;
                    case "1": tedhenat = tedhenat.Where(i => i.ProductName.ToString().StartsWith(Search)); break;
                    case "2": tedhenat = tedhenat.Where(i => i.Description.ToString().StartsWith(Search)); break;
                }
            }
            return PartialView("_Shitja",  tedhenat.ToList());
        }

        [HttpPost]
        public ActionResult ShiteProduktin(string _Barcode)//Shite
        {
            if (_Barcode == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            if (String.IsNullOrEmpty(_Barcode))
            {
                return HttpNotFound();
            }
            int _iBarcode = Convert.ToInt32(_Barcode);
            int id = db.ProductStock.Where(c => c.Barcode == _iBarcode).Select(c => c.ProdID).FirstOrDefault();
            
            ProductStock _prStockDB = db.ProductStock.Find(id);

            if (_prStockDB ==null)
            {
                return HttpNotFound();
            }
            ProductStock_cust prCostum = new ProductStock_cust()
            {
                ProdID = _prStockDB.ProdID,
                Barcode = _prStockDB.Barcode,
                ProductName = _prStockDB.ProductName,
                Description = _prStockDB.Description,
                Quantity = _prStockDB.Quantity,
                UnitPrice = _prStockDB.UnitPrice,
                IsDeleted = _prStockDB.IsDeleted,
                DateRegister = _prStockDB.DateRegister,
                DateModified = _prStockDB.DateModified
            };
            return View(prCostum);
        }

        [HttpPost]
        public ActionResult KonfirmojShitjen([Bind(Include = "ProdID,Barcode,ProductName,Description,Quantity,UnitPrice,IsDeleted,DateRegister,DateModified,QuantitySold")]ProductStock_cust _pr)
        {
            //if (ModelState.IsValid)
            //{
            try
            {
                ProductStock _prDBStock = new ProductStock() 
                {
                    ProdID = _pr.ProdID,
                    Barcode = _pr.Barcode,
                    ProductName = _pr.ProductName,
                    Description = _pr.Description,
                    Quantity = (double)(_pr.Quantity - _pr.QuantitySold),
                    UnitPrice = _pr.UnitPrice,
                    IsDeleted = _pr.IsDeleted,
                    DateRegister = _pr.DateRegister,
                    DateModified = DateTime.Now
                };
                db.Entry(_prDBStock).State = EntityState.Modified;

                //ProductStock _prDB = db.ProductStock.Find(@ViewBag.ProdID);
                //_prDB.Quantity -= Convert.ToDouble(Quantity);

                ProductSold prSold = new ProductSold()
                {
                    ProdID = _pr.ProdID,
                    Quantity = (double)_pr.QuantitySold,
                    UnitPrice = _pr.UnitPrice,
                    TotalPrice = Convert.ToDecimal(_pr.QuantitySold) * _pr.UnitPrice,
                    DateSold = DateTime.Now
                };

                db.ProductSold.Add(prSold);
                db.SaveChanges();
            }
            catch (Exception ex /* dex */)
            {
                ModelState.AddModelError("", "E pamundur per t'u edituar. Provo me vone, nese ka probleme te reja kontakto me Adminin. Faleminderit!");
            }
            //}
            return RedirectToAction("TeShiturat");
        }

        public ActionResult TeShiturat()
        {
            var shitjet = from r in db.ProductSold select r;
            return View(shitjet.ToList());
        }
        [HttpPost]
        public async Task<ActionResult> KerkoTeShiturat(string Search, string searchBy)
       
        {
            var shitjet = from r in db.ProductSold select r;
            if (!String.IsNullOrEmpty(Search) || !String.IsNullOrEmpty(searchBy))
            {
                Search = Search.Trim();
                searchBy = searchBy.Trim();
                switch (searchBy)
                {
                    case "0": shitjet = shitjet.Where(i => i.ProductStock.Barcode.ToString().StartsWith(Search)); break;
                    case "1": shitjet = shitjet.Where(i => i.ProductStock.ProductName.ToString().StartsWith(Search)); break;
                    case "2": shitjet = shitjet.Where(i => i.ProductStock.Description.ToString().StartsWith(Search)); break;
                }
            }
            return View("_TeShitura",await shitjet.ToListAsync());
        }

        public ActionResult Edito(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSold _prDB = db.ProductSold.Find(id);
            if (String.IsNullOrEmpty(id.ToString()))
            {
                return HttpNotFound();
            }

            return View(_prDB);
        }

        [HttpPost, ActionName("Edito")]
        [ValidateAntiForgeryToken]
        public ActionResult EditConfirmed([Bind(Include = "SoldID,ProdID, Barcode,Quantity,UnitPrice,TotalPrice,DateSold")]ProductSold _pr)
        {
            try
            {
                db.Entry(_pr).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("TeShiturat");
            }
            catch (Exception ex /* dex */)
            {
                ModelState.AddModelError("", "E pamundur per t'u edituar. Provo me vone, nese ka probleme te reja kontakto me Adminin. Faleminderit!");
            }

            return View(_pr);
        }
        public ActionResult Detale(int id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSold _pr = db.ProductSold.Find(id);
            if (_pr == null)
            {
                return HttpNotFound();
            }
            return View(_pr);
        }

        public ActionResult Fshije(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ProductSold _pr = db.ProductSold.Find(id);
            if (_pr == null)
            {
                return HttpNotFound();
            }
            return View(_pr);
        }

        // POST: 
        [HttpPost, ActionName("Fshije")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ProductSold _pr = db.ProductSold.Find(id);
            try
            {
                db.Entry(_pr).State = EntityState.Deleted;
                db.SaveChanges();
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "E pamundur per t'u fshire. Provo me vone,nese ka probleme te reja kontakto me Adminin. Faleminderit!");
            }
            return RedirectToAction("TeShiturat");
        }

    }
}