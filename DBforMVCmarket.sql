USE [master]
GO
/****** Object:  Database [dbPBC]    Script Date: 7/4/2018 3:13:49 PM ******/
CREATE DATABASE [dbPBC]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbPBC', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\dbPBC.mdf' , SIZE = 5120KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'dbPBC_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\dbPBC_log.ldf' , SIZE = 2048KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [dbPBC] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbPBC].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbPBC] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbPBC] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbPBC] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbPBC] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbPBC] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbPBC] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbPBC] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbPBC] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbPBC] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbPBC] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbPBC] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbPBC] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbPBC] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbPBC] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbPBC] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbPBC] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbPBC] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbPBC] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbPBC] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbPBC] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbPBC] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbPBC] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbPBC] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbPBC] SET  MULTI_USER 
GO
ALTER DATABASE [dbPBC] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbPBC] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbPBC] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbPBC] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [dbPBC] SET DELAYED_DURABILITY = DISABLED 
GO
USE [dbPBC]
GO
/****** Object:  Table [dbo].[ProductSold]    Script Date: 7/4/2018 3:13:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductSold](
	[SoldID] [int] IDENTITY(1,1) NOT NULL,
	[ProdID] [int] NOT NULL,
	[Quantity] [float] NOT NULL,
	[UnitPrice] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[DateSold] [datetime] NOT NULL,
 CONSTRAINT [PK_ProductSold] PRIMARY KEY CLUSTERED 
(
	[SoldID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ProductStock]    Script Date: 7/4/2018 3:13:49 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductStock](
	[ProdID] [int] IDENTITY(1,1) NOT NULL,
	[Barcode] [int] NOT NULL,
	[ProductName] [nvarchar](90) NOT NULL,
	[Description] [nvarchar](150) NULL,
	[Quantity] [float] NOT NULL,
	[UnitPrice] [decimal](18, 2) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[DateRegister] [datetime] NOT NULL,
	[DateModified] [datetime] NULL,
	[QuantitySold] [float] NULL,
 CONSTRAINT [PK_ProductStock] PRIMARY KEY CLUSTERED 
(
	[ProdID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[ProductSold] ON 

INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (2, 2, 1, CAST(458.00 AS Decimal(18, 2)), CAST(458.00 AS Decimal(18, 2)), CAST(N'2017-06-22 10:02:12.000' AS DateTime))
INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (3, 2, 33, CAST(458.00 AS Decimal(18, 2)), CAST(15114.00 AS Decimal(18, 2)), CAST(N'2017-06-23 12:35:36.400' AS DateTime))
INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (4, 2, 23, CAST(458.00 AS Decimal(18, 2)), CAST(10534.00 AS Decimal(18, 2)), CAST(N'2017-06-23 12:35:59.237' AS DateTime))
INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (5, 4, 3, CAST(31.00 AS Decimal(18, 2)), CAST(93.00 AS Decimal(18, 2)), CAST(N'2017-06-23 12:36:24.623' AS DateTime))
INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (6, 3, 3, CAST(4.00 AS Decimal(18, 2)), CAST(12.00 AS Decimal(18, 2)), CAST(N'2017-06-23 14:00:47.247' AS DateTime))
INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (7, 5, 1, CAST(150.00 AS Decimal(18, 2)), CAST(150.00 AS Decimal(18, 2)), CAST(N'2017-06-24 12:29:44.177' AS DateTime))
INSERT [dbo].[ProductSold] ([SoldID], [ProdID], [Quantity], [UnitPrice], [TotalPrice], [DateSold]) VALUES (10, 10, 12, CAST(72.00 AS Decimal(18, 2)), CAST(864.00 AS Decimal(18, 2)), CAST(N'2017-07-22 11:26:56.783' AS DateTime))
SET IDENTITY_INSERT [dbo].[ProductSold] OFF
SET IDENTITY_INSERT [dbo].[ProductStock] ON 

INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (2, 234976, N'Dell Latitude', N'Laptop i7', 456, CAST(458.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-21 11:02:12.000' AS DateTime), CAST(N'2017-06-23 12:35:59.180' AS DateTime), NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (3, 285252, N'Maus', N'Laserik', 21, CAST(42.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-21 11:02:12.000' AS DateTime), CAST(N'2017-06-28 13:57:25.943' AS DateTime), NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (4, 521879, N'Keyboard', N'Razer', 0, CAST(31.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-22 19:17:06.000' AS DateTime), CAST(N'2017-06-28 13:56:54.233' AS DateTime), NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (5, 259047, N'Camera', N'32Mpx', 9, CAST(150.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-22 19:25:31.000' AS DateTime), CAST(N'2017-06-28 09:15:47.207' AS DateTime), NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (6, 456098, N'Scanner', N'DeScript', 20, CAST(70.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-23 09:16:42.000' AS DateTime), CAST(N'2017-06-23 09:17:08.860' AS DateTime), NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (7, 455612, N'Tablet', N'Mini', 23, CAST(501.00 AS Decimal(18, 2)), 1, CAST(N'2017-06-23 10:41:26.053' AS DateTime), NULL, NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (8, 123563, N'Pad', N'Mouse pad', 12, CAST(45.00 AS Decimal(18, 2)), 1, CAST(N'2017-06-24 12:26:27.303' AS DateTime), NULL, NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (10, 213122, N'Dock 1', N'Station', 2, CAST(72.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-28 12:45:35.000' AS DateTime), CAST(N'2017-07-22 11:27:58.960' AS DateTime), NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (11, 213123, N'dasd22', N'dasda2', 21, CAST(21.00 AS Decimal(18, 2)), 0, CAST(N'2017-06-28 12:46:59.473' AS DateTime), NULL, NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (12, 213121, N'asdasd', N'asda', 21, CAST(12.00 AS Decimal(18, 2)), 1, CAST(N'2017-06-28 12:47:36.813' AS DateTime), NULL, NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (13, 332112, N'sad', N'asd', 21, CAST(12.00 AS Decimal(18, 2)), 1, CAST(N'2017-06-28 13:31:24.757' AS DateTime), NULL, NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (14, 212122, N'sada', N'dsada', 2, CAST(2.00 AS Decimal(18, 2)), 1, CAST(N'2017-06-28 13:31:40.873' AS DateTime), NULL, NULL)
INSERT [dbo].[ProductStock] ([ProdID], [Barcode], [ProductName], [Description], [Quantity], [UnitPrice], [IsDeleted], [DateRegister], [DateModified], [QuantitySold]) VALUES (15, 212123, N'Watch', N'10x10 cm', 5, CAST(7.00 AS Decimal(18, 2)), 0, CAST(N'2017-07-22 15:43:52.293' AS DateTime), NULL, NULL)
SET IDENTITY_INSERT [dbo].[ProductStock] OFF
ALTER TABLE [dbo].[ProductSold]  WITH CHECK ADD  CONSTRAINT [ch_ProdID] FOREIGN KEY([ProdID])
REFERENCES [dbo].[ProductStock] ([ProdID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ProductSold] CHECK CONSTRAINT [ch_ProdID]
GO
USE [master]
GO
ALTER DATABASE [dbPBC] SET  READ_WRITE 
GO
